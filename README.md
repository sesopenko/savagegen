# [From the Table Top Savage Generator](http://fromthetabletop.net)

This is my personal project to learn backbone and require.js.  I want to improve my front-end javascript development skills.

It's based off HTML5 Boilerplate and the tutorial [Organizing your application using Modules](http://backbonetutorials.com/organizing-backbone-using-modules/)

* [RPG Blog](http://fromthetabletop.net)


## Features

* Currently there are no features.


## Documentation

* there is no documentation

