/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/5/13
 * Time: 4:16 PM
 * (c) Sean Esopenko 2013
 */
define([
    'jquery-ui',
    'models/skill', 'collections/skills'
], function($,SkillModel, SkillCollection){
    var skills = {
        boating:['Boating','agility'],
        climbing:['Climbing','strength'],
        driving:['Driving','agility'],
        fighting:['Fighting','agility'],
        gambling:['Gambling','smarts'],
        healing:['Healing','smarts'],
        intimidation:['Intimidation','spirit'],
        investigation:['Investigation','smarts'],
        knowledge:['Knowledge','smarts'],
        lockpicking:['Lockpicking','agility'],
        notice:['Notice','smarts'],
        persuasion:['Persuasion','spirit'],
        piloting:['Piloting','agility'],
        repair:['Repair','smarts'],
        riding:['Riding','agility'],
        shooting:['Shooting','agility'],
        stealth:['Stealth','agility'],
        streetwise:['Streetwise','smarts'],
        survival:['Survival', 'smarts'],
        swimming:['Swimming','agility'],
        taunt:['Taunt','smarts'],
        throwing:['Throwing','agility'],
        tracking:['Tracking','smarts']
    };
    var skillList = [];
    $.each(skills, function(skillId, skill){
        skillList.push(new SkillModel({id:skillId,name:skill[0],attribute:skill[1]}));
    });
    var skillCollection = new SkillCollection(skillList);
    return skillCollection;
});