/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 9:22 PM
 * (c) Sean Esopenko 2013
 */
define(['models/race', 'backbone', 'models/trait','collections/traits', 'collections/races'],
    function(RaceModel, Backbone, TraitModel, TraitCollection, RaceCollection){
    var races = [
        new RaceModel({
            id:'android',
            name:'Android',
            description:'Androids are sentient machines with a variety of appearances depending on the setting.',
            traits:new TraitCollection([
                new TraitModel({
                    name:'Asimov Circuits',
                    description:'The android cannot harm, or by inaction bring harm to sentient beings.  This gives him the Pacifist Hindrance (Major)',
                    freeHindrance:['pacifistmajor']
                }),
                new TraitModel({
                    name:'Construct',
                    description:"Androids are overall quite tough.  They aren't easily shaken, don't have wound modifiers, but can only be repaired, not healed.",
                    shakenRecoverAdj:2,
                    poisonImmunity:true,
                    diseaseImmunity:true,
                    repairHeals:true
                }),
                new TraitModel({
                    name:"Programming",
                    description:"free d6 in one attribute.",
                    diceUpgradeChoice:6
                }),
                new TraitModel({
                    name:"Recharge",
                    description:"Player must determine power source.  Must access power source each day.",
                    userNotes:"Power Source"
                }),
                new TraitModel({
                    name:"Unatural",
                    description:"Arcane powers, both detrimental and beneficial, suffer -2 penalty to affect an android",
                    arcaneAttackAdj:-2
                })
            ])
        }),
        new RaceModel({
            id:'atlantean',
            name:'Atlantean',
            description:"From the crushing depths come the mysterious folk known as Atlanteans",
            traits:new TraitCollection([
                new TraitModel({
                    name:"Advanced Civilization",
                    description:"Generally more intelligent than other races.  Smarts starts at d6.",
                    diceUpgrade:{smarts:6}
                }),
                new TraitModel({
                    name:"Aquatic",
                    description:"Atlanteans live and breathe water",
                    freeSkill:{swimming:6}
                }),
                new TraitModel({
                    name:"Dehydration",
                    description:"Atlanteans must immerse themselves in water one our out of every 24."
                }),
                new TraitModel({
                    name:"Tough",
                    description:"The pressure of their deep homes make Atlanteans tougher than most.",
                    toughtnessAdj:1
                })
            ])
        }),
        new RaceModel({
            id:"human",
            name:"Human",
            description:"Humans are versatile and adaptable.  They get a free edge.",
            traits:new TraitCollection([
                new TraitModel({
                    name:"Free Edge",
                    description:"Humans are versatile and adaptable compared to other races and get a free edge.",
                    freeEdgeChoice:true
                })
            ])
        })
    ];
    var raceCollection = new RaceCollection(races);
    return raceCollection;
});