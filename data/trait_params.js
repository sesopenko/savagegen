/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 8:53 PM
 * (c) Sean Esopenko 2013
 */
define(['jquery','underscore'
], function($,_){
    //I don't think I'm going to use this
    var trait_types = {
        free_edge:0,
        pseudonym_edge:1, //copies edge, different name
        hindrance:2, //takes the specified hindrance
        pseudonym_hindrance:3, //copies hindrance, different name
        trait_choice:4, //choose between traits
        role_playing:5, //role play as described
        shaken_recover_adj:6, //shaken recovery roll adjustment
        poison_immunity:7,
        disease_immunity:8,
        ability_adj:9,
        ability_upgrade_choice:10, //upgrade ability of choice
        attacker_arcane_adjust:11, //when attacked by arcane, adjust rolls by this
        free_skill:12, //gains specified skill for free
        swimming_speed_adj:13,
        toughness_mod:14,
        flight_speed:15,
        pace_adj:16,
        low_light_vision:17,
        infravision:18,
        benny_adj:19,
        size_adj:20,
        cha_adj:21,
        racial_enemy:22, //rakshashans, I think.
        natural_attack:23, //str+d[number] damage
        climbing_adj:24,
        notice_adj:25,
        cold_env_adj:26, //adjustment to cold environmental affects (saurian)
        skill_adj:27, //adjust skill dice
        fatigue_adj:28, //fatigue check adjustment
        shoot_adj:29, //adjust shoot skill (could just use skill adj?)
        notice_adj:30,
        notice_range_adj:31, //adjust notice if beyond specified range
        cha_adj_scenario:33, //cha adjustment in specified scenarios
        common_know_adj:34, //common knowledge check adjustment
        fear_adj:35,
        notice_fail_situations:36, //situations with an automatic fail for notice checks
        running_roll_adj:37,
        climb_adj:38,
        trait_roll_adj_situations:39, //in certain situations, adjust trait roll
        start_cash_adj:40, //percentage adjustment
        income_adj:41, //percentage adjustment
        start_atribute_adj:42, //change starting attributes (ie: 3 instead of 5 for kids)
        start_skill_adj:43, //change starting skill (ie: 10 instead of 15 for kids)
        carry_adj:44, //change the maximum carry multiplier (ie:  8 instead of 5)
        heal_check_adj:45,
        user_notes:46, //user fills out details, such as favoured enemy, thing they're afraid of
        htoh_dam_adj:47 //hand 2 hand damage adjustment
    };
    var trait_requirements = {
        incompatibility:1, //incompatible with specified trait
        rank:2, //requires character rank or higher
        ability:3, //requires specified ability die or higher
        skill:4, //requires specified ranks in skill
        trait:5 //requires specified trait
    }
    var trait_params = {
        types:trait_types,
        requirements:trait_requirements,
        get_typename:function(type){
            for(var type_key in trait_types){
                var this_type = trait_types[type_key];
                if(type == this_type){
                    return type_key.toString();
                }
            }
        }
    };
    return trait_params;
});