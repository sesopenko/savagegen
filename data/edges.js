/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 5:51 PM
 * (c) Sean Esopenko 2013
 */
define([
    'backbone',
    'models/edge', 'app/trait', 'collections/edges'
], function(Backbone, EdgeModel, Trait, EdgeCollection){
    //just a single collection of edges to choose from
    var edges = [
        new EdgeModel({
            id:'alertness',
            name:'Alertness',
            type:'background',
            desc:"Not much gets by your hero.  He's very observant and perceptive, and adds +2 to his Notice rolls to " +
                "hear, see or otherwise sense the world around him."
        }),
        new EdgeModel({
            id:'ambidextrous',
            name:'Ambidextrous',
            abilityReq:{
                agility:8
            },
            type:'background',
            desc:"Your hero is as deft with his left hand as he is with his right.  Characters normally suffer a -2" +
                " penalty when performing physical tasks with the off-hand.  With this Edge, your warrior ignores the" +
                " -2 penalty for using an off-hand."
        }),
        new EdgeModel({
            id:'arcanebackground',
            name:'Arcane Background',
            type:'background',
            desc:"This is the Edge your character must purchase to have any sort of magical, psionic, or other " +
                "supernatural ability."
        }),
        new EdgeModel({
            id:'arcaneresistance',
            name:'Arcane Resistance',
            abilityReq:{
                spirit:8
            },
            type:'background',
            desc:"This individual is particularly resistant to magic.  He acts as if he had 2 points of Armor when hit " +
                "by damaga-causing arcane powers and adds +2 to his trait rolls when resisting opposed powers. " +
                "Even friendly arcane powers must subtract this modifier to affect the resistant hero."
        }),
        new EdgeModel({
            id:'improvedarcaneresistance',
            name:'Improved Arcane Resistance',
            edgeReq:[
                'improvedarcaneresistance'
            ],
            type:'background'
        }),
        new EdgeModel({
            id:'attractive',
            name:'Attractive',
            abilityReq:{
                vigor:6
            },
            type:'background'
        }),
        new EdgeModel({
            id:'veryattractive',
            name:'Very Attractive',
            edgeReq:['attractive'],
            type:'background'
        }),
        new EdgeModel({
            id:'berserk',
            name:'Berserk',
            type:'background'
        }),
        new EdgeModel({
            id:'brave',
            name:'Brave',
            abilityReq:{spirit:6},
            type:'background'
        }),
        new EdgeModel({
            id:'brawny',
            name:'Brawny',
            abilityReq:{strength:6, vigor:6},
            type:'background'
        }),
        new EdgeModel({
            id:'fasthealer',
            name:'Fast Healer',
            abilityReq:{vigor:8},
            type:'background'
        }),
        new EdgeModel({
            id:'fleetfooted',
            name:'Fleet-Footed',
            abilityReq:{agility:6},
            type:'background'
        }),
        new EdgeModel({
            id:'linguist',
            name:'Linguist',
            abilityReq:{smarts:6},
            type:'background'
        }),
        new EdgeModel({
            id:'luck',
            name:'luck',
            type:'background'
        }),
        new EdgeModel({
            id:'greatluck',
            name:'Great Luck',
            edgeReq:['luck'],
            type:'background'
        }),
        new EdgeModel({
            id:'noble',
            name:'Noble',
            type:'background'
        }),
        new EdgeModel({
            id:'quick',
            name:'Quick',
            abilityReq:{agility:8},
            type:'background'
        }),
        new EdgeModel({
            id:'rich',
            name:'Rich',
            type:'background'
        }),
        new EdgeModel({
            id:'filthyrich',
            name:'Filthy Rich',
            edgeReq:['rich','noble'],
            edgeReqOr:true,
            type:'background'
        }),
        new EdgeModel({
            id:'block',
            name:'Block',
            rankReq:'seasoned',
            skillReq:{
                'fighting':8
            },
            type:'fighting'
        }),
        new EdgeModel({
            id:'improvedblock',
            name:'Improved Block',
            rankReq:'veteran',
            edgeReq:['block'],
            type:'fighting'
        }),
        new EdgeModel({
            id:'brawler',
            name:'Brawler',
            abilityReq:{strength:8},
            type:'fighting'
        }),
        new EdgeModel({
            id:'bruiser',
            name:'Bruiser',
            rankReq:'veteran',
            edgeReq:['brawler'],
            type:'fighting'
        }),
        new EdgeModel({
            id:'combatreflexes',
            name:'Combat Reflexes',
            rankReq:'seasoned',
            type:'fighting'
        }),
        new EdgeModel({
            id:'counterattack',
            name:'Counter Attack',
            rankReq:'seasoned',
            skillReq:{fighting:8},
            type:'fighting'
        }),
        new EdgeModel({
            id:'improvedcounterattack',
            name:'Improved Counter Attack',
            rankReq:'veteran',
            edgeReq:['counterattack'],
            type:'fighting'
        }),
        new EdgeModel({
            id:'dodge',
            name:'Dodge',
            rankReq:'seasoned',
            abilityReq:{agility:8},
            type:'fighting'
        }),
        new EdgeModel({
            id:'improveddodge',
            name:'Improved Dodge',
            rankReq:'veteran',
            edgeReq:['dodge'],
            type:'fighting'
        }),
        new EdgeModel({
            id:'elan',
            name:'Elan',
            abilityRank:{spirit:8},
            type:'fighting'
        }),
        new EdgeModel({
            id:'extraction',
            name:'Extraction',
            abilityReq:{agility:8},
            type:'fighting'
        }),
        new EdgeModel({
            id:'improvedextraction',
            name:'Improved Extraction',
            edgeReq:['extraction'],
            type:'fighting'
        }),
        new EdgeModel({
            id:'firststrike',
            name:'First Strike',
            abilityReq:{agility:8},
            type:'fighting'
        }),
        new EdgeModel({
            id:'improvedfirststrike',
            name:'Improved First Strike',
            rankReq:'heroic',
            edgeReq:['firststrike'],
            type:'fighting'
        }),
        new EdgeModel({
            id:'florentine',
            name:'Florentine',
            abilityReq:{agility:8},
            skillReq:{fighting:8},
            type:'fighting'
        }),
        new EdgeModel({
            id:'frenzy',
            name:'Frenzy',
            rankReq:'veteran',
            skillReq:{fighting:10}
        })

    ];
    var edgeCollection = new EdgeCollection(edges);
    return edgeCollection;
});