/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 7:35 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone','models/hindrance', 'collections/hindrances'],
    function(_, Backbone, HindranceModel, HindranceCollection){
    var hindrances = [
        new HindranceModel({
            'id':'allthumbs',
            name:'All Thumbs',
            type:'minor'
        }),
        new HindranceModel({
            id:'anemic',
            name:'Anemic',
            type:'minor'
        }),
        new HindranceModel({
            id:'arrogant',
            name:'Arrogant',
            type:'major'
        }),
        new HindranceModel({
            id:'badeyesminor',
            name:'Bad Eyes (Minor)',
            type:'minor'
        }),
        new HindranceModel({
            id:'badeyesmajor',
            name:'Bad Eyes (Major)',
            type:'major'
        }),
        new HindranceModel({
            id:'pacifistminor',
            name:'Pacifist, Minor',
            type:'minor'
        }),
        new HindranceModel({
            id:'pacifistmajor',
            name:'Pacifist, Major',
            type:'major'
        })
    ];

    var hindranceCollection = new HindranceCollection(hindrances);
    return hindranceCollection;

});