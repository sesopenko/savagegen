/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/2/13
 * Time: 4:31 PM
 * (c) Sean Esopenko 2013
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'router'
], function($, _, Backbone, Router){
    var initialize = function(){
        Router.initialize();
    }
    return {
        initialize:initialize
    };
});