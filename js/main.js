
if(!Window.console){Window.console = {};}
if(!Window.console.log){Window.console.log = function(){};}

require.config({
    paths:{
        jquery:'vendor/jquery/jquery',
        underscore:'vendor/underscore/underscore',
        backbone:'vendor/backbone/backbone',
        templates:'../templates',
        data:'../data',
        'jquery-ui':'vendor/jquery/jquery-ui',
        JSON:'vendor/json/json2'
    },
    shim:{
        'JSON':{
            exports:'JSON'
        },
        'jquery-ui':{
            exports:'$',
            deps:['jquery']
        },
        'backbone':{
            deps:['underscore','jquery'],
            exports:'Backbone'
        },
        'underscore':{
            exports:'_'
        },
        'bootstrap':['jquery']
    }
});

require([
    'app'
], function(App){
    App.initialize();
});