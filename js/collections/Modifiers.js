/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 5:39 PM
 * (c) Sean Esopenko 2013
 */
define([
    'underscore','backbone','models/modifier'
], function(_, Backbone, ModifierModel){
    var ModifiersCollection = Backbone.Collection.extend({
        defaults:{
            model:ModifierModel
        },
        model:ModifierModel
    });
    return ModifiersCollection;
});