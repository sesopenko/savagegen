/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 4:51 PM
 * (c) Sean Esopenko 2013
 */
define([
    'underscore','backbone','models/race'
], function(_, Backbone, RaceModel){
    var RaceCollection = Backbone.Collection.extend({
        defaults:{
            model:RaceModel
        },
        model:RaceModel
    });
    return RaceCollection;
});