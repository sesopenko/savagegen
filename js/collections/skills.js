/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/5/13
 * Time: 4:14 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone','models/skill'],
function(_, Backbone, SkillModel){
    var SkillCollection = Backbone.Collection.extend({
        defaults:{
            model:SkillModel
        },
        model:SkillModel
    });
    return SkillCollection;
})