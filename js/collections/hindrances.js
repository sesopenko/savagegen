/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 8:35 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone','models/hindrance'], function(_, Backbone, HindranceModel){
    var HindranceCollection = Backbone.Collection.extend({
        defaults:{
            model:HindranceModel
        },
        model:HindranceModel
    });
    return HindranceCollection;
});