/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 8:33 PM
 * (c) Sean Esopenko 2013
 */
define(['backbone', 'models/edge'
], function(Backbone, EdgeModel){
    var EdgeCollection = Backbone.Collection.extend({
        defaults:{
            model:EdgeModel
        },
        model:EdgeModel
    });
    return EdgeCollection;

});