/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 8:11 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone','models/trait'], function(_, Backbone, TraitModel){
    var TraitCollection = Backbone.Collection.extend({
        defaults:{
            model:TraitModel
        },
        model:TraitModel
    });
    return TraitCollection;
});