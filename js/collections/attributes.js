/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 2:07 PM
 * (c) Sean Esopenko 2013
 */
define([
    'underscore','backbone','models/attribute'
], function(_, Backbone, AttributeModel){
    //DEPRECATED
    var AttributesCollection = Backbone.Collection.extend({
        defaults:{
            model:AttributeModel
        },
        model:AttributeModel
    });
    return AttributesCollection;
});