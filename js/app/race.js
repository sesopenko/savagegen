/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 9:32 PM
 * (c) Sean Esopenko 2013
 */
define(['app/race_feature'], function(RaceFeature){
    function Race(name, description){
        if(!(this instanceof Race)){
            throw new TypeError("Race constructor cannot be called as a function.");
        }
        this.name = name;
        if(description){
            this.description = description;
        } else {
            this.description = "";
        }
    }
    Race.prototype = {
        constructor:Race,
        set_description:function(description){
            this.description = description;
        },
        add_feature:function(description, traits){

        },
        toString:function(){
            return this.name.toString();
        }
    };
    return Race;
});