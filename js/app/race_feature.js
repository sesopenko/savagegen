/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 9:37 PM
 * (c) Sean Esopenko 2013
 */
define(['app/trait'], function(Trait){
    function RaceFeature(name, description, traits){
        this.name = name;
        this.description = description;
        this.traits = traits;
    }
    RaceFeature.prototype = {
        constructor:RaceFeature,
        toString:function(){
            return this.name.toString();
        }
    }
    return RaceFeature;
});