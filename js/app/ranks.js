/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 5:56 PM
 * (c) Sean Esopenko 2013
 */
define(
    ['underscore'],
    function(_){
        function Ranks(){
            this.ranks = {
                novice:{
                    name:'Novice',
                    minXp:0
                },
                seasoned:{
                    name:'Seasoned',
                    minXp:20
                },
                veteran:{
                    name:'Veteran',
                    minXp:40
                },
                heroic:{
                    name:'Heroic',
                    minXp:60
                },
                legendary:{
                    name:'Legendary',
                    minXp:80
                }
            };
        }

        Ranks.prototype = {
            constructor:Ranks,
            get_rank:function(name){
                var rankName = (typeof name == 'object' && name.name) ? name.name.toLowerCase() : name.toLowerCase();
                var rank = this.ranks[rankName];
                return _.clone(rank);

            },
            xpToRank:function(xp){
                var highest_rank = null;
                _.each(this.ranks, function(rank){
                    if(xp < rank.minXp){
                        highest_rank = _.clone(rank);
                    }
                });
                return highest_rank;
            }
        }
        var ranks = new Ranks();
        return ranks;
    }
);