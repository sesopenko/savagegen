/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 9:39 PM
 * (c) Sean Esopenko 2013
 */
define(['data/trait_params'], function(trait_params){
    function Trait(name, trait_id, value){
        this.name = name;
        this.trait_id = trait_id;
        this.value = value;
    }

    Trait.prototype = {
        constructor:Trait,
        toString:function(){
            return this.name.toString();
        }
    }
    return Trait;
});