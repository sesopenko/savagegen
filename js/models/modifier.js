/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 5:37 PM
 * (c) Sean Esopenko 2013
 */
define([
    'underscore','backbone'
], function(_, Backbone){
    var ModifierModel = Backbone.Model.extend({
        defaults:{

        }
    });
    return ModifierModel;
});