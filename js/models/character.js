/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 8:41 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone',
    'data/skills',
    'models/race',
    'collections/edges', 'collections/hindrances', 'collections/traits'],
    function(_, Backbone,
             skillChoices,
             RaceModel,
             EdgeCollection, HindranceCollection, TraitCollection){
    var CharacterModel = Backbone.Model.extend({
        defaults:{
            race:null,
            userNotes:{},
            diceUpgradeChoice:null,
            savedRace:false,
            abilityPoints:5,
            minAbility:4,
            maxAbility:12, //maximum ability dice
            spentAbility:{},
            savedAbility:false,
            skillPoints:15,
            maxSkill:12, //maximum skill dice
            /*
                Object containing key/value pairs of skills.  each contains an array of the historical skill
                point expenditure.
                ie: {
                    fighting:[1,1,2,2,1], //5 ranks, 7 skill points spent
                    driving:[1,1,1,1] //4 ranks, 4 points spent
                }
             */
            spentSkills:[],
            savedSkills:false,
            savedTraits:false,
            xp:0

        },
        initialize:function(params){
            //do nothing
            this.set({'edges':new EdgeCollection(),'hindrances':new HindranceCollection()});
        },
        totalAbility:function(ability){
            var total = 0;
            if(ability){
                //total only the one
                if(this.get('spentAbility')[ability]){
                    return this.spentAbility[ability];
                }
            } else {
                //total them all
                _.each(this.get('spentAbility'), function(spent){
                    total += spent;
                });
            }
            return total;
        },
        spentAllAbility:function(){
            var total_spent = this.totalAbility();
            if(total_spent >= this.get('abilityPoints')){
                return true;
            } else {
                return false;
            }
        },
        atAbilityMax:function(ability){
            var abilityValue = this.abilityValue(ability);
            return abilityValue >= this.get('maxAbility');
        },
        atAbilityMin:function(ability){
            var abilityValue = this.abilityValue(ability);
            var minAbility = this.get('minAbility');
            var racialValue = this.racialValue(ability);
            if(racialValue > minAbility){
                minAbility = racialValue;
            }
            return abilityValue <= minAbility;
        },
        racialValue:function(ability){
            var value = this.get('minAbility');
            if(this.get('diceUpgradeChoice')){
                var upgradeChoice = this.get('diceUpgradeChoice')[ability];
                if(upgradeChoice > value){
                    value = upgradeChoice;
                }
            }
            if(this.get('race')){
                var racialTraits = this.get('race').get('traits');
                if(racialTraits){
                    var abilityTraits = new TraitCollection(racialTraits.filter(function(trait){
                        if(trait.get('diceUpgrade')[ability]){
                            return true;
                        }
                    }));
                    if(abilityTraits.length > 0){
                        var maxTrait = abilityTraits.max(function(trait){
                            return trait.get('diceUpgrade')[ability];
                        });
                        if(maxTrait.get('diceUpgrade')[ability] > value){
                            value = maxTrait.get('diceUpgrade')[ability];
                        }
                    }
                }
            }
            return value;
        },
        abilityValue:function(ability){
            //start with racial trait
            var value = this.racialValue(ability);
            //TODO:  calculate spent points from character creation
            var spentAbility = this.get('spentAbility')[ability];
            if(spentAbility){
                value += spentAbility*2;
            }
            if(value > this.get('maxAbility')){
                value = this.get('maxAbility');
            } else if(value < this.get('minAbility')){
                value = this.get('minAbility');
            }
            return value;

        },
        adjustAbility:function(ability, amount){
            var spentAbility = _.clone(this.get('spentAbility'));
            var currentValue = spentAbility[ability] || 0;
            var newValue = currentValue + amount;
            spentAbility[ability] = newValue;
            this.set({spentAbility:spentAbility});
        },
        atSkillMin:function(skillId){
            var skillValue = this.skillValue(skillId);
            return !skillValue;
        },
        atSkillMax:function(skillId){
            var skillValue = this.skillValue(skillId);
            if(!skillValue){
                return false;
            }
            return skillValue >= this.get('maxSkill');
        },
        skillPointsRequired:function(skillId){
            var skillValue = this.skillValue(skillId);
            if(!skillValue){
                return 1;
            }
            var skill = skillChoices.findWhere({id:skillId});
            var abilityValue = this.abilityValue(skill.get('attribute'));
            if(skillValue < abilityValue){

                return 1;
            } else {
                return 2;
            }
        },
        skillValue:function(skillId){
            var spentSkills = this.get('spentSkills');
            var numRanks = 0;
            _.each(spentSkills, function(spent){
                var skill = spent[0];
                if(skill == skillId){
                    numRanks++;
                }
            });
            if(numRanks){
                return 2 + (numRanks * 2)
            } else {
                return null;
            }
        },
        spentAllSkill:function(){
            var skillPointsAvailable = this.get('skillPoints');
            var spent = this.totalSkillsSpent();
            return spent >= skillPointsAvailable;
        },
        totalSkillsSpent:function(skillId){
            var totalSpent = 0;
            var spentSkills = this.get('spentSkills');
            _.each(spentSkills, function(skill){
                if(skillId ){
                    if(skillId == skill[0]){
                        totalSpent += skill[1];
                    }
                } else {
                    totalSpent += skill[1];
                }
            });
            return totalSpent;
        },
        skillPointsSpent:function(skillId){
            return this.totalSkillsSpent(skillId);
        },
        adjustSkill:function(skillId, direction){
            var spentSkills = _.clone(this.get('spentSkills'));
            if(direction > 0){
                //add to array
                var requiredPoints = this.skillPointsRequired(skillId);
                spentSkills.push([skillId, requiredPoints]);
                this.set('spentSkills',spentSkills);
            } else {
                var lastIndex = null;
                for(var i = 0; i < spentSkills.length; i++){
                    var spent = spentSkills[i];
                    if(spent[0] == skillId){
                        lastIndex = i;
                    }
                }
                if(!isNaN(parseInt(lastIndex))){
                    //pop this one
                    spentSkills.splice(lastIndex, 1);
                    this.set('spentSkills', spentSkills);
                }
            }
        },
        getRank:function(){
            var xp = this.get('xp');
            var highestRank = null;
            _.each(CharacterModel.ranks, function(rank){
                if(xp >= rank.minXp){
                    highestRank = rank.id;
                }
            });
            return highestRank;
        },
        isRank:function(targetRank){
            var currentRank = this.getRank();
            var metRank = false;
            var passedRank = false;
            _.each(CharacterModel.ranks, function(rank){
                if(!passedRank && rank == targetRank){
                    metRank = true;
                }
                if(rank.id == currentRank){
                    passedRank = true;
                }
            });
            return metRank;
        },
        freeRacialEdges:function(){
            var race = this.get('race');
            var availableEdges = 0;
            if(race){
                var freeRaceEdges = race.get('traits').where({'freeEdgeChoice':true});
                console.log('free edges due to race:',freeRaceEdges);
                console.log('num traits:', freeRaceEdges.length);
                availableEdges += 1;
            }
            availableEdges -= this.get('edges').length;
            return availableEdges;
        },
        availableEdges:function(){
            //calculate the edges that the character can pick
            //check based on the race
            var availableEdges = this.freeRacialEdges();
            var hindrancePoints = 0;
            this.get('hindrances').each(function(hindrance){
                if(hindrance.get('type') == 'major'){
                    hindrancePoints += 2;
                } else {
                    hindrancePoints += 1;
                }
            });
            //determine if we've gone over our skills allotment
            if(hindrancePoints){
                //determine if we've used any due to
            }
            return availableEdges;
        },
        availableHindrance:function(){
            var hindrancePoints = 0;
            this.get('hindrances').each(function(hindrance){
                if(hindrance.get('type') == 'major'){
                    hindrancePoints += 2;
                } else {
                    hindrancePoints += 1;
                }
            });
            //determine if we've gone over our skills allotment
            if(hindrancePoints){
                //determine if we've used any due to edges
                var edgesTraded = this.get('edges').length - this.freeRacialEdges();
                if(edgesTraded > 0){
                    hindrancePoints -= edgesTraded * 2; //worth 2 points each
                }
                //determine if we've used any due to skills
                var skillsSpent = this.skillPointsSpent();
                var skillsLeft = this.get('skillPoints') - this.skillPointsSpent();
                if(skillsLeft < 0){
                    skillsLeft = Math.abs(skillsLeft);
                    hindrancePoints -= skillsLeft;
                }
                //determine if we've used any on ability die
                var abilitySpent = this.totalAbility();


            }
            return hindrancePoints;
        },
        availableHindrances:function(){
            var available = {
                major:1,
                minor:2
            };
            return available;
        }
    });
    CharacterModel.ranks = [
        {
            id:'novice',
            name:'Novice',
            minXp:0
        },
        {
            id:'seasoned',
            name:'Seasoned',
            minXp:20
        },
        {
            id:'veteran',
            name:'Veteran',
            minXp:40
        },
        {
            id:'heroic',
            name:'Heroic',
            minXp:60
        },
        {
            id:'legendary',
            name:'Legendary',
            minXp:80
        }
    ];
    return CharacterModel;
})