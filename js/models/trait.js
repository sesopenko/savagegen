/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 7:44 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone'], function(_, Backbone){
    var TraitModel = Backbone.Model.extend({
        defaults:{
            name:'',
            description:'',
            userNotes:false,
            freeEdge:[],
            freeEdgeChoice:false,
            freeHindrance:[],
            shakenRecoverAdj:0,
            poisonImmunity:false,
            diseaseImmunity:false,
            abilityDieAdj:{},//ie:{agility:1}, note it's not increments of 2.  it's dice upgrades so multiply it,
            abilityCheckAdj:{},
            diceUpgrade:{},//ie:{agility:6}
            diceUpgradeChoice:null,//dice player gets to upgrade one attribute to
            freeSkill:{}, //skill name and dice it's upgraded to
            swimSpeed:null,
            toughnessAdj:0,
            flightSpeed:null,
            paceAdj:0,
            runAdj:null,//changes the run dice
            runPenaltyAdj:0,
            lowLightVision:false,
            infravision:false,
            bennyAdj:0,
            sizeAdj:0,
            charismaAdj:0,
            arcaneAttackAdj:0,
            racialEnemy:null,
            naturalAttack:null, //provide an object: {type:'claws',ability:'strength',dice:8
            climbingAdj:0,
            noticeAdj:0,
            coldEnvironAdj:0,
            skillAdj:{},//ie: {fighting:4,shoot:2}
            fatigueAdj:0,
            incompatibleTrait:null, //can be string or [] filled with strings
            commonKnowAdj:0,
            fearAdj:0,
            startMoneyAdj:1.0,
            incomeAdj:1.0,
            carryAdj:null, //ie:  8x instead of 5x
            healAdj:0,
            parryAdj:0,
            bareHandDamAdj:0,
            bareHandDice:null,
            repairHeals:false
        }
    });
    return TraitModel;
});