/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 4:50 PM
 * (c) Sean Esopenko 2013
 */
define([
    'underscore','backbone',
    'models/modifier',
    'collections/traits'
], function(_, Backbone, ModifierModel, TraitCollection){
    var RaceModel = Backbone.Model.extend({
        defaults:{
            traits:null
        },
        initialize:function(){
        }
    });
    return RaceModel;
});