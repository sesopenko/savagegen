/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/5/13
 * Time: 4:11 PM
 * (c) Sean Esopenko 2013
 */
define([
    'underscore', 'backbone'
], function(_, Backbone){
    var SkillModel = Backbone.Model.extend({
        defaults:{

        },
        initialize:function(){

        }
    });
    return SkillModel;
})