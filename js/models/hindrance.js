/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 7:34 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone'], function(_, Backbone){
    var HindranceModel = Backbone.Model.extend({
        defaults:{
            type:'minor'
        }
    });
    return HindranceModel;
});