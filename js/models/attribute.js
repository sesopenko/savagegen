/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 2:05 PM
 * (c) Sean Esopenko 2013
 */
define([
    'underscore','backbone'
], function(_, Backbone){
    var AttributeModel = Backbone.Model.extend({
        defaults:{
            dice:4
        },
        parse:function(response){
            return response.name;
        },
        initialize:function(model){
        },
        draw_dice:function($element){
            $element.empty().append('d'+this.get('dice').toString());
        }
    });
    return AttributeModel;
});