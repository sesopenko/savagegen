/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/4/13
 * Time: 5:52 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','backbone'], function(_, Backbone){
    var EdgeModel = Backbone.Model.extend({
        defaults:{
            rankReq:'novice',
            skillReq:{},
            edgeReq:[],
            edgeReqOr:false,
            abilityReq:{},
            description:"",
            type:"",
            traits:[]
        }
    });
    return EdgeModel;
});