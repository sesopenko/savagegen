/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/2/13
 * Time: 4:32 PM
 * (c) Sean Esopenko 2013
 */
define([
    // These are path alias that we configured in our bootstrap
    'jquery',     // lib/jquery/jquery
    'underscore', // lib/underscore/underscore
    'backbone'    // lib/backbone/backbone
], function($, _, Backbone){
    // Above we have passed in jQuery, Underscore and Backbone
    // They will not be accessible in the global scope
    return {};
    // What we return here will be used by other modules
});