/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/3/13
 * Time: 1:53 PM
 * (c) Sean Esopenko 2013
 */
define(['jquery-ui','underscore','backbone','JSON',
    'models/attribute', 'models/race', 'models/character',
    'data/races', 'data/edges', 'data/hindrances', 'data/skills',
    'collections/attributes','collections/races',
    'text!data/attributes.json', 'text!data/races.json'
], function($, _, Backbone, JSON,
            AttributeModel, RaceModel, CharacterModel,
            raceChoices, edgeChoices, hindranceChoices ,skillChoices,
            AttributesCollection, RacesCollection,
            attributeData, racesData){
    attributeData = JSON.parse(attributeData);
    racesData = JSON.parse(racesData);
    var TraitView = Backbone.View.extend({
        el:$('div.current_trait', $('div#genedges')),
        initialize:function(character, $parent){
            console.log('constructor params', character, $parent);
            this.$parent = $parent;
            this.character = character;
            this.template = $('script#edgetemplate').html();
        },
        render:function(edge, hindrance){
            var $el = $('div.current_trait', $('div#genedges'), this.$parent);
            var thisView = this;
            var character = this.character;
            var data = {
                edge:edge,
                hindrance:hindrance,
                character:character
            };
            console.log('data:', data);
            var compiledTemplate = _.template(this.template, data);
            $el.empty().append(compiledTemplate);
            this.interactivity();
            return this;
        },
        interactivity:function(){
            var $el = $('div.current_trait', $('div#genedges'), this.$parent);
            $('button.add_edge', $el).button({

            });
        }
    });
    var GenView = Backbone.View.extend({
        el:$('#container'),
        initialize:function(constructor_params){
            this.character = new CharacterModel();
            this.character.on('change', this.render, this);
            this.genTemplate = $('script#gen').html();
            this.tabs = {
                race:0,
                attributes:1,
                skills:2,
                traits:3
            };
            this.selectedTab = this.tabs.race; //the tab that's currently being edited
            this.traitChoices = {
                edges:0,
                hindrances:1
            };
            this.selectedTrait = this.traitChoices.edges; //the trait that's currently being edited
            this.traitView = new TraitView(this.character, this.$el);
        },
        render:function(){
            console.log('rendering:', this.character);
            var thisView = this;
            var data = {
                character:this.character,
                raceChoices:raceChoices,
                edgeChoices:edgeChoices,
                attributeData:attributeData,
                skillChoices:skillChoices,
                hindranceChoices:hindranceChoices
            };
            var compiledTemplate = _.template(this.genTemplate, data);
            this.$el.empty().append(compiledTemplate);
            this.interactivity();
            this.traitView.render();
            return this;
        },

        interactivity:function(){
            var thisView = this;
            var $el = this.$el;
            $('div.tabs', $el).tabs({
                collapsible:false,
                active:this.selectedTab
            });

            this.racialInteractivity();
            this.abilityInteractivity();
            this.skillInteractivity();
            this.traitInteractivity();
        },
        racialInteractivity:function(){
            var thisView = this;
            var $el = this.$el;
            var $choices = $('div#genrace div.race_choices', $el);
            $choices.buttonset();
            if(this.character.get('savedRace')){
                $('input[name=racechoice]', $choices).each(function(){
                    $(this).button('option', 'disabled', true);
                });
            }
            $('div.race_choices input', $el).click(function(){
                var raceId = $(this).val();
                //find the race
                var race = raceChoices.findWhere({id:raceId}).clone();
                thisView.selectedTab = thisView.tabs.race;
                thisView.character.set({race:race});
            });

            $('button[name=save_race]', $el).button({
                icons:{
                    primary:"ui-icon-locked"
                }
            }).click(function(){
                thisView.selectedTab = thisView.tabs.race;
                thisView.saveRace();
            });
        },
        abilityInteractivity:function(){
            var thisView = this;
            var character = thisView.character;
            var savedAbility = character.get('savedAbility');
            var $el = this.$el;
            $('button.increase_attribute', $('div#genattributes'), $el).each(function(){
                var attribute = $(this).attr('data-attribute');
                $(this).button({
                    icons:{
                        primary:'ui-icon-plus'
                    },
                    disabled:character.atAbilityMax(attribute) || character.spentAllAbility() || savedAbility,
                    text:false
                }).click(function(){
                        var attribute = $(this).attr('data-attribute');
                        thisView.selectedTab = thisView.tabs.attributes;
                        thisView.character.adjustAbility(attribute, 1);
                    });
            });
            $('button.decrease_attribute', $('div#genattributes'), $el).each(function(){
                var attribute = $(this).attr('data-attribute');
                $(this).button({
                    icons:{
                        primary:'ui-icon-minus'
                    },
                    disabled:character.atAbilityMin(attribute) || savedAbility,
                    text:false
                }).click(function(){
                        var attribute = $(this).attr('data-attribute');
                        thisView.selectedTab = thisView.tabs.attributes;
                        thisView.character.adjustAbility(attribute, -1);
                    });
            });
            $('button[name=save_attribute]', $('div#genattributes',$el)).button({
                disabled:!character.spentAllAbility() || savedAbility,
                icons:{
                    primary:"ui-icon-locked"
                }
            }).click(function(){
                    thisView.selectedTab = thisView.tabs.attributes;
                    character.set({savedAbility:true});
                });
        },
        skillInteractivity:function(){
            var thisView = this;
            var character = thisView.character;
            var spentAllSkill = character.spentAllSkill();
            var savedSkills = character.get('savedSkills');
            var skillRemaining = character.get('skillPoints') - character.totalSkillsSpent();
            var $el = this.$el;
            $('button.increase_skill', $('div#genskills', $el)).each(function(){
                var skillId = $(this).attr('data-skill');
                $(this).button({
                    icons:{
                        primary:'ui-icon-plus'
                    },
                    disabled:savedSkills || spentAllSkill || character.skillPointsRequired(skillId) > skillRemaining || character.atSkillMax(skillId),
                    text:false
                });
            }).click(function(){
                    var skillId = $(this).attr('data-skill');
                    thisView.selectedTab = thisView.tabs.skills;
                    character.adjustSkill(skillId, 1);
                });
            $('button.decrease_skill', $('div#genskills', $el)).each(function(){
                var skillId = $(this).attr('data-skill');
                $(this).button({
                    icons:{
                        primary:'ui-icon-minus'
                    },
                    disabled:savedSkills || character.atSkillMin(skillId),
                    text:false
                });
            }).click(function(){
                    thisView.selectedTab = thisView.tabs.skills;
                    console.log('decrease skill');
                    var skillId = $(this).attr('data-skill');
                    console.log('value before decrease:', character.skillValue(skillId))
                    character.adjustSkill(skillId, -1);

                });
            $('button[name=save_skills]', $('div#genskills', $el)).button({
                disabled:!character.spentAllSkill() || savedSkills,
                icons:{
                    primary:'ui-icon-locked'
                }
            }).click(function(){
                    thisView.selectedTab = thisView.tabs.skills;
                    character.set({savedSkills:true});
                });

        },
        traitInteractivity:function(){
            var thisView = this;
            var character = thisView.character;
            var skillRemaining = character.get('skillPoints') - character.totalSkillsSpent();
            var $el = this.$el;
            var $accordion = $('div.accordion', $('div#genedges', $el));
            $accordion.accordion({
                active:this.selectedTrait,
                collapsible:true,
                icons:{
                    header:'ui-icon-triangle-1-e',
                    activeHeader:'ui-icon-triangle-1-s'
                }
            });
            $('a.view_edge', $('div#genedges', $el)).click(function(){
                var edgeId = $(this).attr('data-edge');
                var chosenEdge = edgeChoices.findWhere({id:edgeId});
                console.log('chosen edge(', edgeId,'):', chosenEdge);
                thisView.traitView.render(chosenEdge, null);
            });
        },
        saveRace:function(){
            var $el = this.$el;
            console.log('saving race:', this.character);
            var savedData = {
                savedRace:true,
                userNotes:this.character.get('userNotes')
            };
            var $diceUpgrade = $('div#genrace select[name=dice_choice]', $el);
            if($diceUpgrade.length > 0){
                var $traitParent = $diceUpgrade.parent().parent();
                console.log("$traitParent:", $traitParent);
                var traitName = $traitParent.attr('data-traitname');
                //get the trait to find out how much it should be upgraded
                console.log('searching for', traitName);
                var diceTrait = this.character.get('race').get('traits').findWhere({name:traitName});
                var upgradeValue = diceTrait.get('diceUpgradeChoice');
                console.log('diceTrait:', diceTrait);
                savedData.diceUpgradeChoice = {};
                savedData.diceUpgradeChoice[$diceUpgrade.val()] = upgradeValue;
            }
            $('div#genrace input[name=user_notes]', $el).each(function(){
                var traitName = $(this).parent().parent().attr('data-traitname');
                var userNote = $(this).val();
                console.log("Note for", traitName, ":", userNote);
                savedData.userNotes[traitName] = userNote;
            });
            console.log('savedData:', savedData);
            this.character.set(savedData);
        }
    });
    return GenView;
});