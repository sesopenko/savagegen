/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/2/13
 * Time: 6:07 PM
 * (c) Sean Esopenko 2013
 */
/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/2/13
 * Time: 6:02 PM
 * (c) Sean Esopenko 2013
 */
define([
    'jquery', 'underscore', 'backbone', 'text!templates/404.html'
], function($, _, Backbone, fourTemplate){
    var FourView = Backbone.View.extend({
        el:$('#container'),
        render:function(){
            var data = {};
            var compiledTemplate = _.template(fourTemplate, data);
            this.$el.empty().append(compiledTemplate);
        }
    });
    return FourView;
});