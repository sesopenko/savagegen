/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/2/13
 * Time: 4:39 PM
 * (c) Sean Esopenko 2013
 */

define([
    'jquery', 'underscore', 'backbone', 'text!templates/about.html'
], function($, _, Backbone, aboutTemplate){
    var AboutView = Backbone.View.extend({
        el:$('#container'),
        render:function(){
            var data = {};
            var compiledTemplate = _.template(aboutTemplate, data);
            this.$el.empty().append(compiledTemplate);
        }
    });
    return AboutView;
});