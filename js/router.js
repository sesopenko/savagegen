/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 10/2/13
 * Time: 4:30 PM
 * (c) Sean Esopenko 2013
 */
define([
    'jquery', 'underscore', 'backbone',
    'views/about', 'views/fourofour', 'views/gen'
], function($, _, Backbone, AboutView, FourView, GenView){
    var AppRouter = Backbone.Router.extend({
        routes:{
            //define some url routes
            '':'showIndex',
            'about':'showAbout',
            //default
            '*actions':'defaultAction'
        }
    });

    var initialize = function(){
        var app_router = new AppRouter;
        app_router.on('route:showIndex', function(actions){
            var indexView = new GenView();
            indexView.render();
        });
        app_router.on('route:showAbout', function(actions){
            var aboutView = new AboutView();
            aboutView.render();
        });
        app_router.on('route:defaultAction', function(actions){
            var fourView = new FourView();
            fourView.render();
        });
        Backbone.history.start();
    };
    return{
        initialize:initialize
    };
});